import tkinter as tk
from tkinter import messagebox
from collections import defaultdict

# Function to read the automaton from a file
def read_automaton(file_name):
    automate = {'alphabet': set(), 'states': set(), 'initial': None, 'finals': set(), 'transitions': defaultdict(dict)}
    with open(file_name, 'r') as file:
        for line in file:
            if line.startswith('alphabet:'):
                automate['alphabet'] = set(line.strip().split(':')[1].split())
            elif line.startswith('states:'):
                automate['states'] = set(line.strip().split(':')[1].split())
                if '' in automate['states']:
                    automate['states'].remove('')
            elif line.startswith('initial:'):
                automate['initial'] = line.strip().split(':')[1].strip()
            elif line.startswith('final:'):
                automate['finals'] = set(line.strip().split(':')[1].split())
            elif line.startswith('transitions:'):
                for transition in file:
                    transition = transition.strip().split()
                    automate['transitions'][transition[0]][transition[1]] = transition[2]
    return automate

# Function to determinize the automaton
def determinize_automaton(automaton):
    new_states = set()
    new_transitions = defaultdict(dict)

    # Function to compute epsilon closure of states
    def epsilon_closure(states):
        closure = set(states)
        for state in states:
            if '' in automaton['transitions'][state]:
                closure |= set(automaton['transitions'][state][''])
        return closure

    # Function to compute transition of states for a given symbol
    def transition(states, symbol):
        result = set()
        for state in states:
            if symbol in automaton['transitions'][state]:
                result |= set(automaton['transitions'][state][symbol])
        return result

    initial_state = epsilon_closure({automaton['initial']})
    new_states.add(tuple(sorted(initial_state)))

    stack = [initial_state]
    while stack:
        states = stack.pop()
        for symbol in automaton['alphabet']:
            new_state = epsilon_closure(transition(states, symbol))
            if new_state and new_state not in new_states:
                new_states.add(tuple(sorted(new_state)))
                stack.append(new_state)
            new_transitions[tuple(sorted(states))][symbol] = new_state

    final_states = set()
    for state in new_states:
        if any(s in automaton['finals'] for s in state):
            final_states.add(state)
    final_states = {tuple(sorted(s)) for s in final_states}

    determinized_automaton = {'alphabet': automaton['alphabet'], 'states': new_states,
                              'initial': tuple(sorted(initial_state)), 'finals': final_states,
                              'transitions': new_transitions}
    return determinized_automaton

# Function to minimize the automaton
def minimize_automaton(automaton):
    accessible_states = {automaton['initial']}
    marked_states = {automaton['initial']}
    unmarked = {state for state in automaton['states'] if state != automaton['initial']}
    changed = True

    while changed:
        changed = False
        for state in marked_states.copy():
            for symbol in automaton['alphabet']:
                if automaton['transitions'][state][symbol] not in accessible_states:
                    accessible_states.add(automaton['transitions'][state][symbol])
                    changed = True
        marked_states |= accessible_states
        unmarked -= accessible_states

    partition = [accessible_states, unmarked]
    new_partition = [accessible_states, unmarked]
    while changed:
        changed = False
        for i in range(len(partition)):
            for symbol in automaton['alphabet']:
                splits = defaultdict(set)
                for state in partition[i]:
                    splits[automaton['transitions'][state][symbol]].add(state)
                if len(splits) > 1:
                    new_partition[i] = set()
                    for val in splits.values():
                        if val:
                            new_partition.append(val)
                            if val & partition[i]:
                                changed = True
                                partition[i] -= val
                                new_partition[i] |= val
                    break
        partition = new_partition

    new_transitions = defaultdict(dict)
    new_states = set()
    new_finals = set()

    for i, states in enumerate(partition):
        for state in states:
            if state in automaton['finals']:
                new_finals.add(i)
            for symbol, next_state in automaton['transitions'][state].items():
                for j, partition_ in enumerate(partition):
                    if next_state in partition_:
                        new_transitions[i][symbol] = j
                        break

    minimized_automaton = {'alphabet': automaton['alphabet'],
                           'states': set(range(len(partition))),
                           'initial': 0,
                           'finals': new_finals,
                           'transitions': new_transitions}
    return minimized_automaton

# Function to recognize a word in the automaton
def recognize_word(automaton, word):
    steps = []
    current_state = automaton['initial']
    steps.append((0, "Initial State =", current_state))
    for idx, letter in enumerate(word):
        next_states = [automaton['transitions'][current_state][letter]] if letter in automaton['transitions'][current_state] else []
        step = f"Step {idx+1}: Letter '{letter}' -> Next States = {next_states if next_states else 'None'}"
        steps.append((idx+1, step))
        if not next_states:
            return False, steps
        current_state = next_states[0]
    return current_state in automaton['finals'], steps

# Function to display steps in the text widget
def display_steps(steps, text_widget):
    text_widget.delete(1.0, tk.END)
    for step in steps:
        text_widget.insert(tk.END, step[1] + '\n')

# Function to verify a word using the automaton
def verify_word(entry, automaton, text_widget):
    word = entry.get()
    recognized, steps = recognize_word(automaton, word)
    if recognized:
        messagebox.showinfo("Result", f"Le mot '{word}' est reconnu par l'automate.")
    else:
        messagebox.showinfo("Result", f"Le mot '{word}' n'est pas reconnu par l'automate.")
    display_steps(steps, text_widget)

# Function to perform automaton determinization
def determinize(automaton, text_widget):
    determinized_automaton = determinize_automaton(automaton)
    text_widget.delete(1.0, tk.END)
    text_widget.insert(tk.END, "Automate Determinisé :\n")
    text_widget.insert(tk.END, f"{determinized_automaton}\n")

# Function to perform automaton minimization
def minimize(automaton, text_widget):
    minimized_automaton = minimize_automaton(automaton)
    text_widget.delete(1.0, tk.END)
    text_widget.insert(tk.END, " Automate minimisé :\n")
    text_widget.insert(tk.END, f"{minimized_automaton}\n")

# Main function to run the application
def main():
    file_name = 'automate.txt'
    automaton = read_automaton(file_name)

    root = tk.Tk()
    root.title("Verification d'un mot par un automate")
    root.geometry("600x400")
    root.resizable(False, False)

    label = tk.Label(root, text="Entrez un Mot:", font=("Arial", 12))
    label.pack(pady=(10, 5))

    entry = tk.Entry(root, font=("Arial", 12))
    entry.pack(pady=5)

    text_widget = tk.Text(root, height=10, width=70, font=("Arial", 10))
    text_widget.pack(pady=10)

    button_verification = tk.Button(root, text="Verifier le mot", command=lambda: verify_word(entry, automaton, text_widget), font=("Arial", 12), bg="#4CAF50", fg="white")
    button_verification.pack(pady=(5, 10), side="left", padx=10)

    button_determinization = tk.Button(root, text="Determiniser l'automate", command=lambda: determinize(automaton, text_widget), font=("Arial", 12), bg="#F74A1D", fg="white")
    button_determinization.pack(pady=(5, 10), side="left", padx=10)

    button_minimization = tk.Button(root, text="Minimiser l'automate", command=lambda: minimize(automaton, text_widget), font=("Arial", 12), bg="#1768E5", fg="white")
    button_minimization.pack(pady=(5, 10), side="left", padx=10)

    root.mainloop()

if __name__ == "__main__":
    main()
