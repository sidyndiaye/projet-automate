import matplotlib.pyplot as plt

def read_automaton(file_name):
    """
    Reads the description of the automaton from a text file.

    Args:
        file_name (str): The name of the file containing the automaton description.

    Returns:
        dict: A dictionary containing information about the automaton.
    """
    automaton = {}  # Initialize an empty dictionary to store automaton information
    with open(file_name, 'r') as file:
        for line in file:
            if line.startswith('alphabet:'):
                automaton['alphabet'] = line.strip().split(':')[1].split()
            elif line.startswith('states:'):
                automaton['states'] = line.strip().split(':')[1].split()
                if '' in automaton['states']:
                    automaton['states'].remove('')  # Remove empty strings if present
            elif line.startswith('initial:'):
                automaton['initial'] = line.strip().split(':')[1].strip()
            elif line.startswith('final:'):
                automaton['finals'] = line.strip().split(':')[1].split()
            elif line.startswith('transitions:'):
                transitions = []
                for transition in file:
                    transition = transition.strip().split()
                    transitions.append((transition[0], transition[1], transition[2]))
                automaton['transitions'] = transitions
    return automaton

def display_automaton(automaton):
    """
    Displays the automaton as a table using matplotlib.

    Args:
        automaton (dict): A dictionary containing information about the automaton.
    """
    cell_text = []
    cell_text.append(automaton['alphabet'])  # Add the alphabet as the first row
    cell_text.append([''])  # Add an empty row between alphabet and states

    # Add states
    cell_text.append(automaton['states'])
    
    # Mark the initial state
    cell_text[-1][automaton['states'].index(automaton['initial'])] = '-> ' + automaton['initial']

    # Mark the final states
    for final_state in automaton['finals']:
        cell_text[-1][automaton['states'].index(final_state)] += ' *'

    # Fill in missing values with empty strings
    for row in cell_text:
        while len(row) < len(automaton['states']):
            row.append('')

    # Display the table
    plt.figure(figsize=(8, 6))
    plt.title('Automaton')
    plt.table(cellText=cell_text,
              colLabels=list(map(str, automaton['states'])),
              loc='center')
    plt.axis('off')
    plt.show()

def main():
    file_name = 'file.txt'  # Replace with the name of your file
    automaton = read_automaton(file_name)
    display_automaton(automaton)

if __name__ == "__main__":
    main()
