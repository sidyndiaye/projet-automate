alphabet: a b c
states: q0 q1 q2 q3
initial: q0
final: q1 q3
transitions:
q0 a q1
q0 b q2
q1 a q3
q0 b q0
q2 c q3
q3 a q2
