package Automate;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class AutomatonReader {

    public static Map<String, Object> readAutomaton(String fileName) {
        Map<String, Object> automaton = new HashMap<>();
        try (BufferedReader file = new BufferedReader(new FileReader(fileName))) {
            String line;
            while ((line = file.readLine()) != null) {
                if (line.startsWith("alphabet:")) {
                    automaton.put("alphabet", Arrays.asList(line.trim().split(":")[1].split("\\s+")));
                } else if (line.startsWith("states:")) {
                    String[] states = line.trim().split(":")[1].split("\\s+");
                    List<String> statesList = new ArrayList<>();
                    for (String state : states) {
                        if (!state.isEmpty()) {
                            statesList.add(state);
                        }
                    }
                    automaton.put("states", statesList);
                } else if (line.startsWith("initial:")) {
                    automaton.put("initial", line.trim().split(":")[1].trim());
                } else if (line.startsWith("final:")) {
                    automaton.put("finals", line.trim().split(":")[1].split("\\s+"));
                } else if (line.startsWith("transitions:")) {
                    List<String[]> transitions = new ArrayList<>();
                    while ((line = file.readLine()) != null) {
                        String[] transition = line.trim().split("\\s+");
                        transitions.add(new String[]{transition[0], transition[1], transition[2]});
                    }
                    automaton.put("transitions", transitions);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return automaton;
    }

    public static void displayAutomaton(Map<String, Object> automaton) {
        // Retrieve information from the automaton map
        List<String> alphabet = (List<String>) automaton.get("alphabet");

        // Handle the case where states may be an array or a list
        Object statesObject = automaton.get("states");
        List<String> states;
        if (statesObject instanceof String[]) {
            states = Arrays.asList((String[]) statesObject);
        } else {
            states = (List<String>) statesObject;
        }

        String initial = (String) automaton.get("initial");
        String[] finals = (String[]) automaton.get("finals");

        // Display basic information about the automaton
        System.out.println("Automaton:");
        System.out.println("Alphabet: " + String.join(", ", alphabet));
        System.out.println("States: " + String.join(", ", states));
        System.out.println("Initial State: " + initial);
        System.out.println("Final States: " + String.join(", ", finals));

        // Display transition information
        List<String[]> transitions = (List<String[]>) automaton.get("transitions");
        System.out.println("Transitions:");
        for (String[] transition : transitions) {
            System.out.println(transition[0] + " -> " + transition[2] + " on " + transition[1]);
        }
    }

    public static void main(String[] args) {
        String fileName = "file.txt";
        Map<String, Object> automaton = readAutomaton(fileName);
        displayAutomaton(automaton);
    }
}
